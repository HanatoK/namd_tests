#!/bin/sh
# NAMD location
NAMD_EXECUTABLE="/home/hanatok/namd/cpu_build/Linux-x86_64-g++/namd2"
if [[ $# -gt 0 ]]; then
  NAMD_EXECUTABLE=$1
fi

# purge old output
rm -f output/*
mkdir -p output

# run test
echo -n "Check if NAMD can run OPC water model in GPU-resident mode... "
$NAMD_EXECUTABLE +p2 ./min2.conf > test.log
retVal=$?

# check if the crossterm energy is greater than 0
if [[ $retVal -eq 0 ]]; then
  tput setaf 2; echo "Passed."; tput sgr0
  rm -f test.log
  exit 0
else
  tput setaf 1; echo "Failed."; tput sgr0
  rm -f test.log
  exit 1
fi
