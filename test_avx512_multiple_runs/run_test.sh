#!/bin/sh
# NAMD location
NAMD_EXECUTABLE="/home/hanatok/namd/cpu_build/Linux-x86_64-g++/namd2"
if [[ $# -gt 0 ]]; then
  NAMD_EXECUTABLE=$1
fi

# purge old output
rm -f output_eq/*
mkdir -p output_eq/

namdlog=test_avx512_out.log

count=0
while [[ $count -lt 100 ]]; do
  echo "Trying to elicit an error, attempt $count"
  $NAMD_EXECUTABLE +p2 ./alad-eq.namd > $namdlog
  if [[ $? -ne 0 ]]; then
    echo "NAMD crashed. You may need to check dmesg."
    break
  fi
done
