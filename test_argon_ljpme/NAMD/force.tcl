proc calcforces {} {
  loadtotalforces f
  loadcoords p
  set step [getstep]
  set previous_step [expr $step - 1]
#   set output_pos_file [open [format "pos_ljpme_%03d.dat" $step] "w"]
#   for {set i 1} {$i < 513} {incr i} {
#     set fxyz $p($i)
#     puts $output_pos_file [format "%15.10f %15.10f %15.10f" [lindex $fxyz 0] [lindex $fxyz 1] [lindex $fxyz 2]]
#   }
#   close $output_pos_file
  if {$step > 0} {
    set output_file [open [format "force_ljpme_%03d.dat" $previous_step] "w"]
    for {set i 1} {$i < 513} {incr i} {
      set fxyz $f($i)
      puts $output_file [format "%15.10f %15.10f %15.10f" [lindex $fxyz 0] [lindex $fxyz 1] [lindex $fxyz 2]]
    }
    close $output_file
#     print $step $f(1)
  }
}
