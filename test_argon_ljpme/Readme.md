# LJ-PME

## Test system

The system for testing is just a box of 512 argon atoms that have only the LJ parameters from https://doi.org/10.1063/1.479848. The box dimension is $24\mathrm{\AA}\times 24\mathrm{\AA}\times 24\mathrm{\AA}$.

## Compile GROMACS in double precision

Please refer to the GROMACS [installation guide](https://manual.gromacs.org/documentation/current/install-guide/index.html) for more information. I use the double precision CPU version of GROMACS to improve the precision for the comparison purpose.

`cmake` options used when building GROMACS:
```
-DGMX_DOUBLE=ON -DGMX_MPI=OFF -DGMX_GPU=OFF
```

## Running GROMACS simulation

The following operations assumes that the GROMACS binary is installed as `/usr/local/gromacs/bin/gmx_d`.

### Prepare the simulation input file

Enter the `GROMACS/` directory and then run

```sh
/usr/local/gromacs/bin/gmx_d grompp -f eq.mdp -c eq.gro -p structure.top -o eq.tpr
```

### Running the simulation

```sh
/usr/local/gromacs/bin/gmx_d mdrun -s ./eq.tpr
```

### Extract the forces

Run

```sh
/usr/local/gromacs_cpu/bin/gmx_d traj -f traj.trr -s eq.tpr -of
```

and then select `0` from the menu. The forces will be saved to `force.xvg`. Since GROMACS uses $\mathrm{kJ}/(\mathrm{mol}\cdot\mathrm{nm})$ as the unit of forces, we need to convert them to $\mathrm{kcal}/(\mathrm{mol}\cdot\mathrm{\AA})$ for comparison with NAMD. This can be done by running the `format_gmx_force.py`.

## Running the NAMD simulation

Compile the `LJ-PME` branch of NAMD and run it with the configuration file `eq.namd` in `NAMD/`. The forces are obtained from the total forces of `tclForces` and written to `force_ljpme_*.dat` files.

## Compute the error between forces from NAMD and GROMACS

Run `compute_error_namd_gmx.py` and the error of each step will be written to `error_namd_gmx.csv`.
