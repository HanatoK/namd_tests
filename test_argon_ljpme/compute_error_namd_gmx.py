#!/usr/bin/env python3
import numpy as np


def compute_max_relative_error(namd_force_file, gmx_force_file):
    namd_force = np.loadtxt(namd_force_file)
    gmx_force = np.loadtxt(gmx_force_file)
    rmse = np.sqrt(np.mean(np.square(namd_force - gmx_force)))
    error = np.abs(namd_force - gmx_force) / np.abs(namd_force)
    max_error = np.max(error)
    mean_error = np.mean(error)
    return max_error, mean_error, rmse


if __name__ == '__main__':
    with open('error_namd_gmx.csv', 'w') as f_output:
        f_output.write('step,max_rel_error,mean_rel_error,rmsd\n')
        for i in range(100):
            namd_force_file = f'NAMD/force_ljpme_{i:03d}.dat'
            gmx_force_file = f'GROMACS/force_ljpme_{i:03d}.dat'
            max_error, mean_error, rmse = compute_max_relative_error(namd_force_file, gmx_force_file)
            print(f'{max_error:15.7e} {mean_error:15.7e} {rmse:15.7e}')
            f_output.write(f'{i},{max_error:.7e},{mean_error:.7e},{rmse:.7e}\n')
