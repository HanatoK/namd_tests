#!/usr/bin/env python3
import numpy as np
k = 0
with open('force.xvg', 'r') as f_input:
    for line in f_input:
        if line.startswith('@') or line.startswith('#'):
            continue
        if line:
            data = np.array(list(map(float, line.split()))[1:]).reshape(-1, 3) / 41.84
            np.savetxt(f'force_ljpme_{k:03d}.dat', data, fmt='%15.10f')
            k += 1
