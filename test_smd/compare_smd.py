#!/usr/bin/env python3
import argparse
import sys
import math


def parse_namd_smd(filename: str):
    records = {}
    with open(filename, 'r') as f_input:
        for line in f_input:
            if line.startswith('SMD '):
                fields = line[len('SMD '):].split()
                records[int(fields[0])] = list(map(float, fields[1:]))
    return records


def compare_records(R1: dict, R2: dict):
    results = {}
    for key in R1.keys():
        diff2 = 0
        for i in range(len(R1[key])):
            diff2 += (R1[key][i] - R2[key][i]) * (R1[key][i] - R2[key][i])
        results[key] = math.sqrt(diff2 / len(R1[key]))
    return results


def check_results(results: dict, threshold=0.01):
    for key in results.keys():
        if results[key] > threshold:
            return False
    return True


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('NAMD_logfile1', help='NAMD log file 1 to compare')
    parser.add_argument('NAMD_logfile2', help='NAMD log file 2 to compare')
    args = parser.parse_args()
    energy_1 = parse_namd_smd(args.NAMD_logfile1)
    energy_2 = parse_namd_smd(args.NAMD_logfile2)
    if len(energy_1) != len(energy_2):
        exit(1)
    results = compare_records(energy_1, energy_2)
    if check_results(results):
        sys.exit(0)
    else:
        sys.exit(1)


if __name__ == '__main__':
    main()
