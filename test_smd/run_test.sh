#!/bin/sh
# NAMD location
NAMD_EXECUTABLE="/home/hanatok/namd/cpu_build/Linux-x86_64-g++/namd2"
if [[ $# -gt 0 ]]; then
  NAMD_EXECUTABLE=$1
fi

# purge old output
rm -f ubq_ww_pcv.*

# run test
echo -n "Check if NAMD can run SMD in GPU-resident mode... "
$NAMD_EXECUTABLE +p2 ./test.conf > gpu_resident.log
retVal=$?

if [[ $retVal -eq 0 ]]; then
  # compare the energies
  python3 ./compare_smd.py gpu_resident.log gpu_no_resident.log
  # check the return code
  retVal=$?
fi

# purge old output
rm -f ubq_ww_pcv.*

# check if the crossterm energy is greater than 0
if [[ $retVal -eq 0 ]]; then
  tput setaf 2; echo "Passed."; tput sgr0
  rm -f gpu_resident.log
  exit 0
else
  tput setaf 1; echo "Failed."; tput sgr0
  rm -f gpu_resident.log
  exit 1
fi
