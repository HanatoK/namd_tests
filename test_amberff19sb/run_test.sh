#!/bin/sh
# NAMD location
NAMD_EXECUTABLE="/home/hanatok/namd/cpu_build/Linux-x86_64-g++/namd2"
if [[ $# -gt 0 ]]; then
  NAMD_EXECUTABLE=$1
fi

# the suffix of the output files
outputname="alad_out"
# purge old output
rm -f $outputname.coor $outputname.coor.BAK $outputname.vel $outputname.vel.BAK $outputname.xsc $outputname.xsc.BAK $outputname.log

# run test
echo -n "Check if NAMD supports CMAP terms in AMBER ff19SB... "
$NAMD_EXECUTABLE +p4 ./alad_min_eq.namd > $outputname.log

# extract the crossterm_energy
crossterm_energy=$(grep '^ENERGY:' $outputname.log|awk '{print $6}')

# check if the crossterm energy is greater than 0
if (( $(echo "$crossterm_energy > 0"|bc -l) )); then
  tput setaf 2; echo "Passed (crossterm energy is $crossterm_energy)."; tput sgr0
  rm -f $outputname.coor $outputname.coor.BAK $outputname.vel $outputname.vel.BAK $outputname.xsc $outputname.xsc.BAK $outputname.log
  exit 0
else
  tput setaf 1; echo "Failed."; tput sgr0
  exit 1
fi
