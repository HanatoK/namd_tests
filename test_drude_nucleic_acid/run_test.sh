#!/bin/sh
# NAMD location
NAMD_EXECUTABLE="/home/hanatok/namd/cpu_build_debug/Linux-x86_64-g++/namd3"
if [[ $# -gt 0 ]]; then
  NAMD_EXECUTABLE=$1
fi

# the suffix of the output files
outputname="test_out"
# purge old output
rm -f $outputname.coor $outputname.coor.BAK $outputname.vel $outputname.vel.BAK $outputname.xsc $outputname.xsc.BAK $outputname.log

# run test
echo -n "Check if NAMD supports 1-4 NbThole correction... "
$NAMD_EXECUTABLE +p4 ./eq.namd > $outputname.log

# extract the potential
potential_energy=$(grep '^ENERGY:' $outputname.log|awk '{print $14}')

# reference potential energy and test threshold from https://github.com/openmm/openmm/pull/2952/files#diff-723208b0ac80eb6f6b8ce094ddec26d86c0cb51b27b3f8479edaacefbd547891
reference_potential_energy=-292.73015
threshold=1.0

error=$(printf "%10.5f" $(echo "sqrt(($potential_energy - $reference_potential_energy)^2)"|bc -l))
# check if the crossterm energy is greater than 0
if (( $(echo "$error < $threshold"|bc -l) )); then
  tput setaf 2; echo "Passed (Error = $error)."; tput sgr0
  rm -f $outputname.coor $outputname.coor.BAK $outputname.vel $outputname.vel.BAK $outputname.xsc $outputname.xsc.BAK $outputname.log
  exit 0
else
  tput setaf 1; echo "Failed (Error = $error)."; tput sgr0
  exit 1
fi
