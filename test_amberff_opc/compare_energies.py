#!/usr/bin/env python3
import argparse
import sys
import math


def parse_namd_energies(filename: str):
    title_parsed = False
    records = {}
    keys = []
    with open(filename, 'r') as f_input:
        for line in f_input:
            if line.startswith('ETITLE:') and title_parsed is False:
                fields = line[len('ETITLE:'):].split()
                keys = fields.copy()
                for key in keys:
                    records[key] = []
                title_parsed = True
            if title_parsed is True:
                if line.startswith('ENERGY:'):
                    fields = line[len('ENERGY:'):].split()
                    for key, val in zip(keys, fields):
                        records[key].append(float(val))
    return records


def compare_records(R1: dict, R2: dict):
    results = {}
    for key in R1.keys():
        diff2 = 0.0
        for x, y in zip(R1[key], R2[key]):
            diff2 += (x - y) * (x - y)
        results[key] = math.sqrt(diff2 / len(R1[key]))
    return results


def check_results(results: dict, threshold=0.1):
    for key in results.keys():
        if results[key] > threshold:
            return False
    return True


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('NAMD_logfile1', help='NAMD log file 1 to compare')
    parser.add_argument('NAMD_logfile2', help='NAMD log file 2 to compare')
    args = parser.parse_args()
    energy_1 = parse_namd_energies(args.NAMD_logfile1)
    energy_2 = parse_namd_energies(args.NAMD_logfile2)
    results = compare_records(energy_1, energy_2)
    if check_results(results):
        sys.exit(0)
    else:
        sys.exit(1)


if __name__ == '__main__':
    main()
